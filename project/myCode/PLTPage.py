from myCode import MyUtils
from myCode import PLTEntry

def getPLTPageInfo(dump):
	PLTPage = dict()
	posInDump = 0
	PLTEntryLength = 32
	dumpLengthInOctets = len(dump) / 2
	pageCount = 0
	while posInDump < dumpLengthInOctets:
		PLTEntrySequence = MyUtils.extractSequence(dump, posInDump, PLTEntryLength)
		PLTEntryInfo = PLTEntry.getPLTEntryInfo(PLTEntrySequence)
		if PLTEntryInfo != 0:
			PLTPage[pageCount] = PLTEntryInfo
		posInDump += PLTEntryLength
		pageCount += 1
	return PLTPage
