from myCode import MyUtils

def getPLTEntryInfo(dump):
	PLTEntryInfo = []
	nbPageUsed = MyUtils.hexToInt(dump[0:2])
	if nbPageUsed == 0:
		return 0
	nbPageUsed -= 128
	currentPageAnalysis = 1

	while currentPageAnalysis <= nbPageUsed:
		dumpPos = currentPageAnalysis * 2
		page = MyUtils.hexToInt(dump[dumpPos:dumpPos+2])
		PLTEntryInfo.append(page)
		currentPageAnalysis += 1
	return PLTEntryInfo
