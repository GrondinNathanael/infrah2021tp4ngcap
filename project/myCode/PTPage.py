from myCode import MyUtils
from myCode import PTEntry

def getPTPage(dump, index):
	PTPage = dict()
	position = 0
	PTEntryLength = 32
	lengthInOctets = len(dump) / 2
	while position < lengthInOctets:
		PTEntrySequence = MyUtils.extractSequence(dump, position, PTEntryLength)
		PTEntryInfo = PTEntry.getPTEntry(PTEntrySequence)
		if PTEntryInfo['pageID'] == index:
			PTPage = PTEntryInfo
			return PTEntryInfo
		position += PTEntryLength
