from myCode import MyUtils

def getPTEntry(dump):
	PTEntry = dict({'ACL': [], 'isSwappedOut': '', 'pageID': '', 'pageLocation': '' })
	swapBinary = toBinary(dump[0:1])
	if(swapBinary[0] == 1):
		PTEntry['isSwappedOut'] = True
	else:
		PTEntry['isSwappedOut'] = False
	position = MyUtils.hexToInt(dump[2:4])
	PTEntry['pageLocation'] = position
	PTEntry['pageID'] = position
	firstPart = 4
	secondPart = 6
	PTEntry['ACL'].append(0)
	while dump[firstPart:secondPart] != '00':
		PTEntry['ACL'].append(MyUtils.hexToInt(dump[firstPart:secondPart]))
		firstPart = firstPart + 2
		secondPart = secondPart + 2
	return PTEntry


def toBinary(hexValue):
        intValue = MyUtils.hexToInt(hexValue)
        binary = []
        binaryNbCounter = 4
        while intValue > 0:
                toBin = intValue % 2
                binary.append(toBin)
                intValue = intValue // 2
                binaryNbCounter -= 1
        while binaryNbCounter > 0:
                binary.append(0)
                binaryNbCounter -= 1
        binary.reverse()
        return binary
