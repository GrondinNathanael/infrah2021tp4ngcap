from myCode import MyUtils

def getBitmapPage(dump):
	pageList = []
	posCounter = 0
	for char in dump:
		if char != '0':
			posChar = posCounter * 4
			binaryArray = toBinary(char)
			posBin = 0
			for binary in binaryArray:
				if binary == 1:
					pos = posChar + posBin
					pageList.append(pos)
				posBin += 1
		posCounter += 1
	return pageList

def toBinary(hexValue):
	intValue = MyUtils.hexToInt(hexValue)
	binary = []
	binaryNbCounter = 4
	while intValue > 0:
		toBin = intValue % 2
		binary.append(toBin)
		intValue = intValue // 2
		binaryNbCounter -= 1
	while binaryNbCounter > 0:
		binary.append(0)
		binaryNbCounter -= 1
	binary.reverse()
	return binary
