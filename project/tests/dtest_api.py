import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout1.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(16, map['pageSize'])
		self.assertEqual(64, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(2, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		list = json.loads(response.content.decode('utf-8'))
		list = list['bitmapInfo']
		expected = [0, 1, 2, 3, 4, 6, 11, 16, 21, 22, 32]

		self.assertEqual(expected, list)

	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode("utf-8"))
		map = map['ptInfo']

		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 0, 'pageLocation': 0 }), map['0'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 1, 'pageLocation': 1 }), map['1'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 2, 'pageLocation': 2 }), map['2'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 3, 'pageLocation': 3 }), map['3'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 4, 'pageLocation': 4 }), map['4'])
		self.assertEqual(dict({'ACL': [0, 30], 'isSwappedOut': False, 'pageID': 6, 'pageLocation': 6 }), map['6'])
		self.assertEqual(dict({'ACL': [0, 30], 'isSwappedOut': True, 'pageID': 11, 'pageLocation': 11 }), map['11'])
		self.assertEqual(dict({'ACL': [0, 44, 8], 'isSwappedOut': False, 'pageID': 16, 'pageLocation': 16 }), map['16'])
		self.assertEqual(dict({'ACL': [0, 8], 'isSwappedOut': False, 'pageID': 21, 'pageLocation': 21 }), map['21'])
		self.assertEqual(dict({'ACL': [0, 30], 'isSwappedOut': False, 'pageID': 22, 'pageLocation': 22 }), map['22'])
		self.assertEqual(dict({'ACL': [0, 8], 'isSwappedOut': False, 'pageID': 32, 'pageLocation': 32 }), map['32'])

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['pltInfo']

		self.assertEqual([0,1,2,3,4], map['0'])
		self.assertEqual([16,32,21], map['8'])
		self.assertEqual([22,6,11], map['30'])
		self.assertEqual([16], map['44'])


if __name__ == "__main__":
	unittest.main()
