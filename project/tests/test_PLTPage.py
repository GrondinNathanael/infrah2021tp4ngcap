import unittest

from myCode import PLTPage

class PLTPageTests(unittest.TestCase):

	def test_getPLTPageInfo(self):
		hexDump = "831020150000000000000000000000000000000000000000000000000000000082542a0000000000000000000000000000000000000000000000000000000000"
		expectedMap = dict()
		expectedMap[0] = [16,32,21]
		expectedMap[1] = [84,42]
		result = PLTPage.getPLTPageInfo(hexDump)

		self.assertEqual(expectedMap[0], result[0])
		self.assertEqual(expectedMap[1], result[1])

	def test_getPLTPageInfo2(self):
		hexDump = "8310201500000000000000000000000000000000000000000000000000000000"
		expectedMap = dict()
		expectedMap[0] = [16,32,21]
		result = PLTPage.getPLTPageInfo(hexDump)

		self.assertEqual(expectedMap[0], result[0])
