import unittest

from myCode import PLTEntry

class PLTEntryTests(unittest.TestCase):

	def test_getPLTEntryInfo(self):
		hexDump = "83102015"
		expectedList = [16,32,21]

		self.assertEqual(expectedList, PLTEntry.getPLTEntryInfo(hexDump))

	def test_getPLTEntryInfo2(self):
		hexDump = "00102015"
		expectedResult = 0

		self.assertEqual(expectedResult, PLTEntry.getPLTEntryInfo(hexDump))

	def test_getPLTEntryInfo3(self):
		hexDump = "000000000000"
		expectedResult = 0

		self.assertEqual(expectedResult, PLTEntry.getPLTEntryInfo(hexDump))
