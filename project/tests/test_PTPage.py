import unittest

from myCode import PTPage
from myCode import PTEntry

class PTPageTests(unittest.TestCase):

	def test_getPTPageInfoSwappedTrue(self):
		hexDump = "8E10201500000000000000000000000000000000000000000000000000000000"
		expectedMap = dict({'ACL': [0, 32, 21], 'isSwappedOut': True, 'pageID': 16, 'pageLocation': 16 })
		result = PTEntry.getPTEntry(hexDump)
		self.assertEqual(result['isSwappedOut'], expectedMap['isSwappedOut'])

	def test_getPTPageInfoSwappedFalse(self):
                hexDump = "1E10201500000000000000000000000000000000000000000000000000000000"
                expectedMap = dict({'ACL': [0, 32, 21], 'isSwappedOut': False, 'pageID': 16, 'pageLocation': 16 })
                result = PTEntry.getPTEntry(hexDump)
                self.assertEqual(result['isSwappedOut'], expectedMap['isSwappedOut'])


	def test_getPTPageInfoACL(self):
                hexDump = "8E10201500000000000000000000000000000000000000000000000000000000"
                expectedMap = dict({'ACL': [0, 32, 21], 'isSwappedOut': True, 'pageID': 16, 'pageLocation': 16 })
                result = PTEntry.getPTEntry(hexDump)
                self.assertEqual(result['ACL'], expectedMap['ACL'])

	def test_getPTPageInfoLocation(self):
                hexDump = "8E10201500000000000000000000000000000000000000000000000000000000"
                expectedMap = dict({'ACL': [0, 32, 21], 'isSwappedOut': True, 'pageID': 16, 'pageLocation': 16 })
                result = PTEntry.getPTEntry(hexDump)
                self.assertEqual(result['pageLocation'], expectedMap['pageLocation'])

	def test_getPTPageInfoID(self):
                hexDump = "8E10201500000000000000000000000000000000000000000000000000000000"
                expectedMap = dict({'ACL': [0, 32, 21], 'isSwappedOut': True, 'pageID': 16, 'pageLocation': 16 })
                result = PTEntry.getPTEntry(hexDump)
                self.assertEqual(result['pageID'], expectedMap['pageID'])

	def test_getPLTPageInfo(self):
                hexDump = "2E222015EE000000000000000000000000000000000000000000000000000000"
                expectedMap = dict()
                expectedMap[34] = dict({'ACL': [0, 32, 21, 238], 'isSwappedOut': False, 'pageID': 34, 'pageLocation': 34 })
                result = PTPage.getPTPage(hexDump, 34)

                self.assertEqual(expectedMap[34], result)
