import unittest
import requests
import json
from myAPI import api

IP = "0.0.0.0"
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	@classmethod
	def setUpClass(cls):
		with open("./project/files/MemoryLayout3.vsml", "rb") as f:
			myFile = {'file':f}
			response = requests.post(URL + "/upload", files=myFile)
			if response.status_code != 200:
				print(response.content.decode())
				raise Exception("SetUpClass failed for APITest")


	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_MetaRoute(self):
		response = requests.get(URL + "/metaInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(8, map['pageSize'])
		self.assertEqual(128, map['nbPhysicalPages'])
		self.assertEqual(0, map['nbSwapPages'])
		self.assertEqual(1, map['nbBitmapPages'])
		self.assertEqual(8, map['nbPTPages'])
		self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmapInfo")
		self.assertEqual(200, response.status_code)
		list = json.loads(response.content.decode('utf-8'))
		list = list['bitmapInfo']
		expected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 28, 42, 84, 100, 101]

		self.assertEqual(expected, list)

	def test_ptRoute(self):
		response = requests.get(URL + "/ptInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode("utf-8"))
		map = map['ptInfo']
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 0, 'pageLocation': 0 }), map['0'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 1, 'pageLocation': 1 }), map['1'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 2, 'pageLocation': 2 }), map['2'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 3, 'pageLocation': 3 }), map['3'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 4, 'pageLocation': 4 }), map['4'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 5, 'pageLocation': 5 }), map['5'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 6, 'pageLocation': 6 }), map['6'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 7, 'pageLocation': 7 }), map['7'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 8, 'pageLocation': 8 }), map['8'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 9, 'pageLocation': 9 }), map['9'])
		self.assertEqual(dict({'ACL': [0], 'isSwappedOut': False, 'pageID': 10, 'pageLocation': 10 }), map['10'])
		self.assertEqual(dict({'ACL': [0,2], 'isSwappedOut': False, 'pageID': 28, 'pageLocation': 28 }), map['28'])
		self.assertEqual(dict({'ACL': [0,3], 'isSwappedOut': False, 'pageID': 42, 'pageLocation': 42 }), map['42'])
		self.assertEqual(dict({'ACL': [0,3], 'isSwappedOut': False, 'pageID': 84, 'pageLocation': 84 }), map['84'])
		self.assertEqual(dict({'ACL': [0,4], 'isSwappedOut': False, 'pageID': 100, 'pageLocation': 100 }), map['100'])
		self.assertEqual(dict({'ACL': [0,5], 'isSwappedOut': False, 'pageID': 101, 'pageLocation': 101 }), map['101'])

	def test_pltRoute(self):
		response = requests.get(URL + "/pltInfo")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['pltInfo']

		self.assertEqual([0,1,2,3,4,5,6,7,8,9,10], map['0'])
		self.assertEqual([28], map['2'])
		self.assertEqual([84,42], map['3'])
		self.assertEqual([100], map['4'])
		self.assertEqual([101], map['5'])


if __name__ == "__main__":
	unittest.main()
