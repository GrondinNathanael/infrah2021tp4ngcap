import unittest

from myCode import BitmapPage

class BitmapPageTests(unittest.TestCase):

	def test_getBitmap(self):
		hexDump = "FFA40003000A020670000000000000000000000000000000000000"
		expectedList = [0,1,2,3,4,5,6,7,8,10,13,30,31,44,46,54,61,62,65,66,67]

		self.assertEqual(expectedList, BitmapPage.getBitmapPage(hexDump))

	def test_toBinary(self):
		hexValue = "F"
		expectedBinary = [1,1,1,1]

		self.assertEqual(expectedBinary, BitmapPage.toBinary(hexValue))

	def test_toBinary2(self):
		hexValue = "8"
		expectedBinary = [1,0,0,0]

		self.assertEqual(expectedBinary, BitmapPage.toBinary(hexValue))

	def test_toBinary3(self):
		hexValue = "0"
		expectedBinary = [0,0,0,0]

		self.assertEqual(expectedBinary, BitmapPage.toBinary(hexValue))

	def test_toBinary4(self):
		hexValue = "2"
		expectedBinary = [0,0,1,0]

		self.assertEqual(expectedBinary, BitmapPage.toBinary(hexValue))
