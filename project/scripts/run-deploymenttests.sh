echo "Running deployment tests with coverage stats"

./runDocker.sh
python3 -m unittest discover -s project -p dtest_*.py -v

if [ $? -ne 0 ]; then
	echo "Deployment tests failed"
	exit 1
fi

echo "Deployment tests done"
