echo "Running pre-commit hook"

echo "Running unittests"
./project/scripts/run-unittests.sh

if [ $? -ne 0 ]; then
	echo "Unit tests must pass before commit"
	exit 1
fi

echo "unittests succeeded"

echo "Running deploymenttests"
./project/scripts/run-deploymenttests.sh

if [ $? -ne 0 ]; then
	echo "Deployment tests must pass before commit"
	exit 1
fi

echo "All tests passed succesfully"
