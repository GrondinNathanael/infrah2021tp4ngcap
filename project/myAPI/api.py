from flask import *

from myCode import MetaPage
from myCode import PageBroker
from myCode import BitmapPage
from myCode import PTPage
from myCode import PLTPage

app = Flask('memoryanalysis')

@app.route('/')
def welcome(): #pragma: no cover
	name = "NG et CAP"

	return """
	<form action="/upload" enctype="multipart/form-data" method="POST">
	<p>
		Welcome to the memory dump analysis API.<br>
		<br> <br>
		This code is provided as a starting point for TP4.
		<br> <br>
		Produced by:""" + name + """
	</p>
	<p>
		Please specify a file to analyze: <br>
		<input type="file" name="file" size="40">
	</p>
	<div>
		<input type="submit" value="Submit">
	</div>
	<p>
		Or analyse the (previously) uploaded file by going to one of the following:</br>
			<ul>
			<li><a href="/metaInfo">metaInfo</a></br></li>
			<li><a href="/bitmapInfo">bitmapInfo</a></br></li>
			<li><a href="/ptInfo">ptInfo</a></br></li>
			<li><a href="/pltInfo">pltInfo</a></br></li>
			</ul>
	</p>
	</form>"""



@app.route('/upload', methods=['POST'])
def upload(): #pragma: no cover
	if 'file' not in request.files:
		return make_response('file missing from upload request.',400)
	file = request.files['file']
	file.save(PageBroker.DUMP_FILE)
	try:
		__getMetaPage()
	except:
		return make_response("The file provided does not appear to be a valid memory dump file.", 400)
	return "file successfully uploaded.</br>Return <a href='/'>home</a>"


def __getMetaPage(): #pragma: no cover
	dumpMetaPage = PageBroker.getPage(0)
	metaPage = MetaPage.parseFromHexDump(dumpMetaPage)
	return metaPage


@app.route('/metaInfo')
def getMetaPageInfo(): #pragma: no cover
	metaPage = __getMetaPage()
	return jsonify({'metaInfo':metaPage.toMap()})

def __getBitmapPage(): #pragma: no cover
	dumpBitmapPage = PageBroker.getPage(1)
	bitmapInfo = BitmapPage.getBitmapPage(dumpBitmapPage)
	return bitmapInfo

@app.route('/bitmapInfo')
def getBitmapPageInfo(): #pragma: no cover
	bitmapInfo = __getBitmapPage()
	return jsonify({'bitmapInfo':bitmapInfo})

def _getPTPageInfo(): #pragma: no cover
	dumpBitmapPage = __getBitmapPage()
	PTPageInfo = dict()
	metaPage = __getMetaPage()
	index = 2
	nbOfPtPages = 2 + metaPage.getNbPageTablePages()
	for x in dumpBitmapPage:
		while index <= nbOfPtPages:
			dumpPTPage = PageBroker.getPage(index)
			PTPageInfo[x] = PTPage.getPTPage(dumpPTPage, x)
			index = index + 1
			if PTPageInfo[x] is not None:
				index = 2
				break
		index = 2
	return PTPageInfo

@app.route('/ptInfo')
def getPTPageInfo(): #pragma: no cover
	PTPageInfo = _getPTPageInfo()
	return jsonify({'ptInfo':PTPageInfo})


def __getPLTPage(): #pragma: no cover
	metaPage = __getMetaPage()
	nbOfPtPages = metaPage.getNbPageTablePages()
	dumpPLTPage = PageBroker.getPage(2 + nbOfPtPages)
	PLTPageInfo = PLTPage.getPLTPageInfo(dumpPLTPage)
	return PLTPageInfo

@app.route('/pltInfo')
def getPLTPageInfo(): #pragma: no cover
	PLTPageInfo = __getPLTPage()
	return jsonify({'pltInfo':PLTPageInfo})


if __name__ == '__main__': #pragma: no cover
	app.run(debug=True, host='0.0.0.0', port = 5555)
