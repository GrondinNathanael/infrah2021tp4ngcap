set -e
coverage run --omit *dtest_userAPI.py,*__init__.py,/usr/*,/home/lubuntu/.local/* -m unittest discover -s ./project -v
coverage report
sh runDocker.sh
sleep 2
python3 -m unittest discover -p dtest_*.py -s ./project -v
docker stop tp4ngcap
