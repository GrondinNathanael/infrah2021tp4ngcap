#!/bin/bash

docker stop tp4ngcap
docker container rm tp4ngcap
docker image rm tp4ngcap_image
docker volume rm tp4ngcap_vol

docker volume create --name tp4ngcap_vol --opt device=$PWD --opt o=bind --opt type=none
docker build -t tp4ngcap_image -f ./project/docker/Dockerfile .
docker run -d -p 5555:5555 --mount source=tp4ngcap_vol,target=/mnt/app/ --name tp4ngcap tp4ngcap_image

